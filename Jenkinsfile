pipeline {
    agent any
    environment {
        appRepository = 'flask-app'  // Name of your Docker image repository
        awsID = 'awsID'  // AWS ID
        awsRegion = 'eu-north-1'  // AWS region
        registryCredential = "ecr:$awsRegion:$awsID"  // Authentification to ECR registry
        ecrRegistry = "211125620953.dkr.ecr.eu-north-1.amazonaws.com" // registry URL
        clusterName = 'my_cluster'
        serviceName = 'flask-web-service'
    }
    stages {
        stage('Build Docker Image') {
            steps {
                script {
                    dockerImage = docker.build(appRepository + ":$BUILD_NUMBER", ".")
                }
            }
        }
        stage('Test') {
            steps {
                script {
                    dockerImage.inside {sh "python -m unittest"}
                }
            }
        }
        stage('Upload Docker Image') {
            steps {
                script {
                    docker.withRegistry('https://' + ecrRegistry, registryCredential) {
                        dockerImage.push("$BUILD_NUMBER")
                        dockerImage.push("latest")
                    }
                }
            }
        }
        stage('Cleanup Docker Images') {
            steps {
                script {
                    sh "docker rmi ${appRepository}:${env.BUILD_NUMBER}"
                    sh "docker rmi ${ecrRegistry}/${appRepository}:${env.BUILD_NUMBER}"
                    sh "docker rmi ${ecrRegistry}/${appRepository}:latest"
                }
            }
        }
        stage('Deploy to ECS') {
            steps {
                withAWS(credentials: awsID, region: awsRegion) {
                    sh 'aws ecs update-service --cluster ${clusterName} --service ${serviceName} --force-new-deployment'
                }
            }
        }
    }
}